package ru.gb.tests.Homework2;

import io.appium.java_client.MobileElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CheckLoginPositiveTest extends BaseTest {
    @Test
    public void CheckLogin() throws Exception {
//      Нажимаем Login в меню
        Thread.sleep(2000);
        MobileElement loginMenuButton = (MobileElement) driver.findElementByAccessibilityId("Login");
        loginMenuButton.click();
        Thread.sleep(2000);
//      Вводим валидный логин
        MobileElement inputEmailField = (MobileElement) driver.findElementByAccessibilityId("input-email");
        inputEmailField.sendKeys("qwe@google.com");
        Thread.sleep(2000);
//      Вводим валидный пароль
        MobileElement passwordField = (MobileElement) driver.findElementByAccessibilityId("input-password");
        passwordField.sendKeys("12345678");
        Thread.sleep(2000);
//      Нажимаем Login на странице логин/пароль.
        MobileElement loginButton = (MobileElement) driver.findElementByAccessibilityId("button-LOGIN");
        loginButton.click();
        Thread.sleep(2000);

        MobileElement errorText = (MobileElement) driver.findElementByXPath("\t\n" +
                "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.TextView");
        Assert.assertEquals(errorText.getText(), "You are logged in!");

    }
}
