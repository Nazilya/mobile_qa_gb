package ru.gb.tests.Homework2;

import io.appium.java_client.MobileDriver;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.net.MalformedURLException;
import java.net.URL;

public class BaseTest {
    MobileDriver driver;

    @BeforeTest
    public void capabilitiesSetup() throws MalformedURLException {
        // Устанавливаем capabilities.
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("deviceName", "Pixel");
        capabilities.setCapability("platformVersion", "10");
        capabilities.setCapability("udid", "emulator-5554");
        capabilities.setCapability("automationName", "UiAutomator2");
//      Проверьте актуальность пути до приложения на своём компьютере.
        capabilities.setCapability("app", "/Users/test/Downloads/Android-nativeDemoApp-0.2.1.apk");
//        Пример для предустановленного приложения.
//        capabilities.setCapability("appPackage", "com.google.android.apps.maps");
//        capabilities.setCapability("appActivity", "com.google.android.maps.MapsActivity");
//        capabilities.setCapability("noReset", true);

//      Устанавливаем и открываем приложение.
        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);
//      Нажимаем Login в меню


    }


    @AfterTest
    void tearDown() {
        driver.quit();
    }
}
