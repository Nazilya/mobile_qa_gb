package ru.gb.tests.Homework2;

import io.appium.java_client.MobileElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SignUpPNegativeTest extends BaseTest {
    @Test
    public void CheckSignUpWithInvalidPassword() throws Exception {

        Thread.sleep(2000);
        MobileElement loginMenuButton = (MobileElement) driver.findElementByAccessibilityId("Login");
        loginMenuButton.click();
        Thread.sleep(2000);

        MobileElement signUPMenuButton = (MobileElement) driver.findElementByXPath("//android.view.ViewGroup[@content-desc=\"button-sign-up-container\"]/android.view.ViewGroup/android.widget.TextView");
        signUPMenuButton.click();
        Thread.sleep(2000);

        MobileElement emailButton = (MobileElement) driver.findElementByAccessibilityId("input-email");
        emailButton.sendKeys("qwe@google.com");
        Thread.sleep(2000);

        MobileElement passwordButton = (MobileElement) driver.findElementByAccessibilityId("input-password");
        passwordButton.sendKeys("12345");
        Thread.sleep(2000);

        MobileElement passwordRepeatButton = (MobileElement) driver.findElementByAccessibilityId("input-repeat-password");
        passwordRepeatButton.sendKeys("12345");
        Thread.sleep(2000);

        MobileElement signUPButton = (MobileElement) driver.findElementByAccessibilityId("button-SIGN UP");
        signUPButton.click();
        Thread.sleep(2000);


        MobileElement signUpText = (MobileElement) driver.findElementById("android:id/message");
        Assert.assertEquals(signUpText.getText(), "Some fields are not valid!");

    }
}
